pragma solidity ^0.4.18;

contract CubeCalculator {
    uint public value;
    
    function SetValue(uint val) public {
        value = val;
    }
    
    function CalculateCube() constant public returns (uint) {
        uint cube = value * value * value;
        assert(cube / value == value); // overflow protection
        return cube;
    }
}