# ThetaBDK #


### What is Theta? ###

The **Theta Network** is a blockchain that specializes in decentralized peer-to-peer video delivery 
rather than the centralized CDNs (Content Delivery Network) used by streaming sites like **YouTube** and **Twitch**.

**Theta** promises higher quality streaming, no video re-buffering, and other enhancements 
over traditional methods of video delivery.

Users share their excess bandwidth to **Theta** and are rewarded with **Theta Tokens** (aka **TFUEL**), 
the native token of the **Theta** blockchain.

For more information:

*  [Theta Website](https://www.thetatoken.org)

*  [Theta GitHub](https://github.com/thetatoken)


### What is ThetaBDK? ###

**ThetaBDK** is a blockchain development kit for **Theta** that contains documentation, 
sample code, extensions, python APIs, smart contracts, wallet templates, and analytical & monitoring tools.


### Getting Started with Theta ###

**STEP 1:  Create a Wallet**

A wallet refers to a program (or physical medium) that stores cryptocurrency or tokens, public and/or private keys,
and addresses related to your particular blockchain account.

The Theta wallet manages **TFUEL** and **Theta Network** accounts. 

* Create a [Theta wallet](https://wallet-beta.thetatoken.org/unlock/keystore-file)

* Click on **Create Wallet**


![alt text](https://bitbucket.org/theta-network/theta-bdk/downloads/01_theta_wallet.JPG)


* Click on **Unlock Wallet**


![alt text](https://bitbucket.org/theta-network/theta-bdk/downloads/02_unlock_wallet.JPG)



* Type in a strong password and click **Download Keystore** to generate keystore file


![alt text](https://bitbucket.org/theta-network/theta-bdk/downloads/03_create_keystore.JPG)


The mnemonic phrase is a 12 word passphrase that allows the recovery of a wallet.

* Click on **Continue** to display the phrase

* Then Click on **View my Private Key** to display the private key


![alt text](https://bitbucket.org/theta-network/theta-bdk/downloads/04_mnemonic_phrase.JPG)


* You are done.  Click on **Unlock Wallet** to access a new Theta wallet.


![alt text](https://bitbucket.org/theta-network/theta-bdk/downloads/07_youaredone.JPG)
